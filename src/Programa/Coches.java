package Programa;

import java.io.File;

public class Coches {

	String marca;
	String matricula;
	String tipo;
	int KM;
	int precio;
	String estado;
	
	File ficheroCoche;
	
	static boolean libre = true;

//	private String nombre_fichero = "C:\\Libres\\bmw1.txt";


	public static void Alquilar(String nombreCoche) {

		// // PASAR COCHES LIBRES A COCHES ALQUILADOS
		try {
			funcionesGeneral.copyFile_JavaAlquilar(nombreCoche, nombreCoche);
		} catch (Exception e) {
			System.out.println("No se ha podido alquilar");
		}

	}

	public static void Liberar(String nombreCoche) {

		// PASAR COCHES ALQUILADOS A COCHES LIBRES
		
		try {
			funcionesGeneral.copyFile_JavaLiberar(nombreCoche, nombreCoche);
		} catch (Exception e) {
			System.out.println("No se ha podido alquilar");
		}
		

	}

}
