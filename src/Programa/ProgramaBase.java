      package Programa;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

public class ProgramaBase {
	
	static Scanner scn = new Scanner(System.in);

	public static void main(String[] args) throws FileNotFoundException, IOException {
		
		int opcion=0;
		String nombrecoche = null;
	//	int numero;
		
		funcionesGeneral.CargarCoche(funcionesGeneral.carpeta_libre);
		funcionesGeneral.CargarCoche(funcionesGeneral.carpeta_alquilado);
		
		do {
			
			opcion = menu(opcion);
			
			
			switch(opcion) {
				case 1:
					
					funcionesGeneral.listarFicherosPorCarpeta(null); 					
					break;
				case 2:
					scn.nextLine();
					System.out.println("introduce lo que quieras alquilar");
					nombrecoche=scn.nextLine();
					Coches.Alquilar(nombrecoche);
					
					break;
				case 3:
					
					scn.nextLine();
					System.out.println("introduce lo que quieras liberar");
					nombrecoche=scn.nextLine();
					Coches.Liberar(nombrecoche);
					
					break;
				case 4:
					scn.nextLine();
					System.out.println("Introduce el Coche que vas a introducir");
					nombrecoche=scn.nextLine();
					funcionesGeneral.CrearCoche(nombrecoche);
					break;
				case 5:
					scn.nextLine();
					System.out.println("Introduce el Coche que vas a introducir");
					nombrecoche=scn.nextLine();
					funcionesGeneral.darDeBaja(nombrecoche);
										
					break;
				case 6:
					scn.nextLine();
					System.out.println("Introduce el coche con el que quieres hacer el presupuesto :");
					nombrecoche=scn.nextLine();
					funcionesGeneral.presupuesto(nombrecoche);
					break;
				case 0:
					break;
			}
		}while(opcion != 0);

	}
	
	public static int menu(int opcio) {

		System.out.println("1.- Mostrar coches ");
		System.out.println("2.- Alquilar coche");
		System.out.println("3.- Liberar coche");
		System.out.println("4.- Declarar coche nuevo");
		System.out.println("5.- Dar de baja un coche ");
		System.out.println("6.- Presupuesto ");
		System.out.println("0.- Sortir");

		opcio = scn.nextInt();

		return opcio;
	}

}
