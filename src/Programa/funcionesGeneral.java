package Programa;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.Buffer;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.HashMap;
import java.util.Scanner;

public class funcionesGeneral {

	public static String carpeta_alquilado = "D:\\Documents\\Eclipse\\AlquilerCochesP\\src\\Programa\\coches\\alquilados\\";
	public static String carpeta_libre = "D:\\Documents\\Eclipse\\AlquilerCochesP\\src\\Programa\\coches\\libres\\";
	public static HashMap<String,Coches> ListaCoches = new HashMap<String, Coches>();
	
	static Scanner scn = new Scanner(System.in);

	

	public static void MostrarCoches(String string) {   

		// MOOSTRAR COCHES LIBRES
		// MOSTRAR COCHES RESERVADOS
		// MOSTRAR TODOS LOS COCHES

	}

	public static void CrearCoche(String nomCoche) {

		// CREAR UN NUEVO COCHE Y UBICARLO EN UNA CARPETA

		String marca;
		String matricula;
		String tipo;
		int KM;
		int precio;
//		String estado;

		System.out.println("Introduce la marca del coche: ");
		marca = scn.nextLine();

		System.out.println("Introduce la matricula del coche: ");
		matricula = scn.nextLine();

		System.out.println("Introduce el tipo del coche: ");
		tipo = scn.nextLine();

		System.out.println("Introduce los KM del coche: ");
		KM = scn.nextInt();

		System.out.println("Introduce el precio del coche: ");
		precio = scn.nextInt();

		scn.nextLine();

//		System.out.println("Introduce el estado del coche: ");
//		estado = scn.nextLine();

		PrintStream ps = null;

		try {
			ps = new PrintStream(carpeta_libre + nomCoche);
			ps.println(marca);
			ps.println(matricula);
			ps.println(tipo);
			ps.println(KM);
			ps.println(precio);
//			ps.println(estado)="Libre";

		} catch (Exception e) {
			System.out.println("No se ha podido crear el coche ");
		} finally {
			ps.close();
		}

	}
	
	public static void CargarCoche(String carpeta) {
		// pide fitchero del coche 
		File folder = new File(carpeta);
		File[] listOfFiles = folder.listFiles();
		
		// por cada fichcero solicita la funcion de Crear coche Objeto para convertirlo en objeto 
		for(int i = 0; i<listOfFiles.length; i++) {
//			System.out.println(listOfFiles[i].getName());
			CrearCocheObjeto(listOfFiles[i]);
			//ArrayList
			
		}
		
	}
	
	public static void CrearCocheObjeto(File archivo) {
		
		// Crea el objedo de coche
		BufferedReader bReader = null;
		try {
			bReader = new BufferedReader(new FileReader(archivo));
			
		}
		catch(Exception e){
			System.out.println("No se ha podido acceder al archivo " + archivo.getAbsolutePath());
		}
		
		
		Coches c = new Coches();
		
		try {
			c.marca=bReader.readLine();
			c.matricula=bReader.readLine();
			c.tipo=bReader.readLine();
			c.KM= Integer.parseInt(bReader.readLine());
			c.precio= Integer.parseInt(bReader.readLine());
//			c.estado=bReader.readLine();
		}catch (Exception e) {
			System.out.println("Error al leer el archivo  " + archivo.getAbsolutePath() );
		}
		
		try {
			bReader.close();
		} catch (Exception e) {
			
		}
		
		ListaCoches.put(c.marca, c);
				
	}

	// DEVUELVE EL PRESUPUESTO PARA UN COCHE CONCRETO SEGUN LOS DIAS QUE INTRODUZCAS
	public static void presupuesto(String coche) {
		int dias=0;
		int preciofin=0;
		// COMPROEBA QUE LOS DIAS SEAN MAYORES QUE 0
		while(dias<1) {
		System.out.println(" Introduce los dias que vas a alquilar el coche : ");
		dias=scn.nextInt();
		}
	
		
//		System.out.println(ListaCoches.get(coche).precio);
		
		System.out.println("****************************"+"\n");
		
		// multiplica los dias introducidos por el precio del coche solicitado
		preciofin=dias*ListaCoches.get(coche).precio;
		
		System.out.println( " El precio final es de :");
		
		System.out.println( " -> " + preciofin);
		
		System.out.println( "\n");
		System.out.println("****************************");

	}

	// MUESTRA EL CONTENIDO DE UN ARCHIVO
	public static void muestraContenido(String archivo) { // throws FileNotFoundException, IOException {
		String cadena;
		
		try {
			FileReader f = new FileReader(archivo);
			BufferedReader b = new BufferedReader(f);
			while ((cadena = b.readLine()) != null) {
				System.out.println(cadena);
			}
			b.close();
			f.close();
		} catch (Exception e) {
			System.out.println("Error al mostrar fichero");
		}
	}

	// ELIMINAR FICHERO
	public static void darDeBaja(String ruta) {		
		
		File fichero = new File(carpeta_libre + ruta);

		//Comprovar si esta en libres
		if (!fichero.exists()) {

			// Comprovar en alquilados
			fichero = new File(carpeta_alquilado + ruta);
	
			// Si no existe en alquilados
			if (!fichero.exists()) {

				// El coche no existe
				System.out.println("El coche no existe.");
				return;

			} else {

				//Avisar de que esta alquilado
				System.out.println("El coche esta alquilado");
				return;
			}
		}
		
		
		//Borrar coche
		if(fichero.delete())
			//Avisar de que se ha borrado
			System.out.println("El archivo fue eliminado.");
		else
			System.out.println("No se ha podido eliminar el archivo");
		
	}
	
	// ELIMINA LA COPIA DE UN ARCHIVO
	private static void eliminarCopia(String origen) {
		
		File fichero = new File(origen);
		
		fichero.delete();
		
	}
	
	
	// COPIA FITCHERO DE UNA CARPETA A OTRA Y ELIMINA EL ORIGEN (PARA ALQUILAR)
	
	public static void copyFile_JavaAlquilar(String origen, String destino) throws IOException {
		
		//Crea los path de origen y destino
        Path FROM = Paths.get(carpeta_libre+origen);
        Path TO = Paths.get(carpeta_alquilado+destino);
        //sobreescribir el fichero de destino, si existe, y copiar
        // los atributos, incluyendo los permisos rwx
        CopyOption[] options = new CopyOption[]{
          StandardCopyOption.REPLACE_EXISTING,
          StandardCopyOption.COPY_ATTRIBUTES
        }; 
        //Copia el fichero de FROM al TO 
        Files.copy(FROM, TO, options);
        eliminarCopia(carpeta_libre+origen);
         
    }
	


	// // PASAR COCHES LIBRES A COCHES ALQUILADOS(PARA LIBERAR)
	
	public static void copyFile_JavaLiberar(String origen, String destino) throws IOException {
		
		//Crea los path de origen y destino
		Path FROM = Paths.get(carpeta_alquilado+origen);
        Path TO = Paths.get(carpeta_libre+destino);
        //sobreescribir el fichero de destino, si existe, y copiar
        // los atributos, incluyendo los permisos rwx
        CopyOption[] options = new CopyOption[]{
          StandardCopyOption.REPLACE_EXISTING,
          StandardCopyOption.COPY_ATTRIBUTES
        }; 
        //Copia el fichero de FROM al TO 
        Files.copy(FROM, TO, options);
        eliminarCopia(carpeta_alquilado+origen);
               
    }

	public static void listarFicherosPorCarpeta(final File carpeta) {
	 
		File folder = new File(carpeta_libre);
		File[] listOfFiles = folder.listFiles();
		
		File folder2 = new File(carpeta_alquilado);
		File[] listOfFiles2 = folder2.listFiles();
		
		System.out.println("----------- Coches Libres -----------");
		
		for (int i = 0; i < listOfFiles.length; i++) {
			  if (listOfFiles[i].isFile()) {
			    System.out.println(listOfFiles[i].getName());
			  }
		}
		
		System.out.println("----------- Coches Alquilados -----------");
		
		for (int i = 0; i < listOfFiles2.length; i++) {
			  if (listOfFiles2[i].isFile()) {
			    System.out.println(listOfFiles2[i].getName());
			  }
		}
		
		System.out.println("\n");
		
	}

}
